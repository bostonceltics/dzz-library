const Nightmare = require("nightmare");
const nightmare = Nightmare({ show: true });

nightmare
  .goto("https://www.futurepedia.io") // Navigate to the specified URL
  .wait("h1") // Wait for the <h1> element to be present on the page
  .evaluate(() => {
    // Extract the text of the <h1> element and return it
    const h1 = document.querySelector("h1");
    return h1.textContent;
  })
  .end() // End the Nightmare instance
  .then((h1Text) => {
    // Display the extracted <h1> text in the console
    console.log("The text of the <h1> element is:", h1Text);
  })
  .catch((error) => {
    // Handle any errors that occur during the scraping process
    console.error("Scraping failed:", error);
  });
