import * as React from "react";
import { Header, Hero, Filters, Catalog, Footer } from "../components/index";
import {
  Pagination,
  PaginationItem,
  CssBaseline,
  Grid,
  Stack,
  Box,
  Typography,
  Container,
} from "@mui/material";

import { ArrowBackIcon, ArrowForwardIcon } from "@mui/icons-material";

export default function Home() {
  return (
    <>
      <CssBaseline />
      <Header />
      <main>
        <Hero />

        <Container xl={{ py: 8 }} maxWidth="xl">
          <Grid container spacing={2}>
            <Grid item xs={3}>
              <Filters />
            </Grid>

            <Grid item xs={9}>
              <Catalog />
            </Grid>
          </Grid>

          <Stack
            spacing={2}
            alignItems="center"
            justifyContent="center"
            style={{ marginTop: "50px" }}
          >
            <Pagination
              count={10}
              renderItem={(item) => (
                <PaginationItem
                  slots={{ previous: ArrowBackIcon, next: ArrowForwardIcon }}
                  {...item}
                />
              )}
            />
          </Stack>
        </Container>
      </main>

      <Footer />
    </>
  );
}
