import React from "react";
import { useSession, signOut } from "next-auth/react";
import { Header, Footer, Comments } from "../../components/index";
import Image from "next/image";
import {
  Pagination,
  PaginationItem,
  CssBaseline,
  Grid,
  Stack,
  Box,
  Typography,
  Container,
  Avatar,
  CardHeader,
  CardActions,
  Rating,
  Checkbox,
  Link,
} from "@mui/material";

import { Favorite, FavoriteBorder } from "@mui/icons-material";

import { red } from "@mui/material/colors";

import postImg from "../../assets/images/book-1.jpeg";

const BookPage = () => {
  const { data, status } = useSession();
  if (status === "loading") return <h1> loading... please wait</h1>;

  return (
    <>
      <CssBaseline />
      <Header />
      <main>
        {status === "authenticated" && (
          <>
            <Box
              sx={{
                bgcolor: "background.paper",
                pt: 12,
                mb: 6,
              }}
            >
              <Container xl={{ py: 8 }} maxWidth="lg">
                <Typography
                  component="h1"
                  variant="h2"
                  align="center"
                  color="text.primary"
                  gutterBottom
                >
                  How to create selling e-commerce websites
                </Typography>
              </Container>
            </Box>

            <Container xl={{ py: 8 }} maxWidth="lg">
              <Grid container spacing={2}>
                <Grid item xs={6}>
                  <Image
                    loader={() => postImg.src}
                    src={postImg.src}
                    width={postImg.width}
                    height={postImg.height}
                    alt={`book-1`}
                  />
                </Grid>

                <Grid item xs={6}>
                  <Stack
                    spacing={2}
                    direction="row"
                    alignItems="center"
                    justifyContent="flex-start"
                    sx={{
                      mb: 1,
                    }}
                  >
                    <Avatar
                      component="a"
                      href="/pavel-tolstyko"
                      sx={{ bgcolor: red[500] }}
                      aria-label="recipe"
                    >
                      R
                    </Avatar>

                    <Rating name="size-small" defaultValue={2} size="small" />
                    <Checkbox
                      aria-label="recipe"
                      icon={<FavoriteBorder />}
                      checkedIcon={<Favorite />}
                    />
                  </Stack>

                  <Stack
                    spacing={2}
                    direction="row"
                    alignItems="center"
                    justifyContent="flex-start"
                    sx={{
                      mb: 1,
                    }}
                  >
                    <Link color="inherit" href="/books">
                      Books
                    </Link>
                    {", "}
                    <Link color="inherit" href="/english-books">
                      English books
                    </Link>
                  </Stack>

                  <Typography variant="body2" color="text.secondary">
                    Designing and developing e-commerce websites is a puzzle
                    with thousands of pieces. Not only are the layout and
                    structure of your Web store important, but you also have to
                    keep the user experience and conversion rates in mind. These
                    are what, in the end, really convince your client’s
                    customers to click that shiny “Buy now” button.
                  </Typography>
                </Grid>
              </Grid>
            </Container>

            <Box
              sx={{
                bgcolor: "background.paper",
                pt: 6,
                mb: 6,
              }}
            >
              <Container xl={{ py: 8 }} maxWidth="lg">
                <p>
                  In this book you’ll be studying the universal principles for
                  successful e-commerce websites, which include improving your
                  check-out process and making your product displays more
                  attractive. In reality, optimizing your conversion rates takes
                  little effort. Find out how to resolve small usability issues
                  to immense effect. Are you familiar with A/B and multivariate
                  testing? Use them to figure out how customers respond to
                  minimal changes in design, content structure and check-out
                  convenience. Customers decide whether to stay on a page in
                  just a few (milli)seconds, so you better make them count!
                </p>
                <p>
                  Only outstanding articles have made it into the book,
                  according to Smashing Magazine’s high-quality standards. Our
                  authors are professionals, and their careful research figures
                  largely in the book. Take the advice of experts who know
                  exactly what they’re writing about.
                </p>
                <h4>TABLE OF CONTENTS</h4>
                <ul>
                  <li>Preface</li>
                  <li>Getting Started With E-Commerce</li>
                  <li>
                    5 Universal Principles For Successful E-Commerce-Sites
                  </li>
                  <li>12 Tips for Designing an Excellent Checkout Process</li>
                  <li>How to Engage Customers in Your E-Commerce Website</li>
                  <li>Principles of Effective Search in E-Commerce Design</li>
                  <li>15 Common Mistakes in E-Commerce Design</li>
                  <li>Fundamentals of a Successful Re-Design</li>
                  <li>
                    Improve Your E-Commerce Design With Brilliant Product Photos
                  </li>
                  <li>How To Use Photos To Sell More Online</li>
                  <li>Design To 8 Useful Tips To Help Your Website Convert</li>
                  <li>7 More Useful Tips To Help Your Site Convert</li>
                  <li>Optimizing Conversion Less Effort, More Customers</li>
                  <li>Optimizing Conversion It’s All About Usability</li>
                  <li>Use Conversions To Generate More Conversions</li>
                  <li>The Ultimate Guide To A/B Testing</li>
                  <li>
                    Multivariate 5 Simple Steps to Increase Conversion Rates
                  </li>
                </ul>

                <Comments />
              </Container>
            </Box>
          </>
        )}
      </main>

      <Footer />
    </>
  );
};

export default BookPage;
