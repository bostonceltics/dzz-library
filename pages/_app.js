import "@/assets/styles/normalize.css";
import "@/assets/styles/global.scss";
// import { StateContext } from "../context/StateContext";

export default function App({ Component, pageProps }) {
  return (
    <>
      {/* <StateContext> */}
      <Component {...pageProps} />
      {/* </StateContext> */}
    </>
  );
}
