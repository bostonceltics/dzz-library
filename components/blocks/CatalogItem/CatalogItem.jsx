import React from "react";
import {
  Typography,
  Checkbox,
  Card,
  CardHeader,
  CardActions,
  CardContent,
  CardMedia,
  Avatar,
  Rating,
  Grid,
  Link,
} from "@mui/material";

import { Favorite, FavoriteBorder } from "@mui/icons-material";

import { red } from "@mui/material/colors";

const CatalogItem = ({ key }) => {
  return (
    <Grid item key={key} xs={12} sm={6} md={4} lg={4}>
      <Card sx={{ maxWidth: 345 }}>
        <CardHeader
          avatar={
            <Avatar
              component="a"
              href="/pavel-tolstyko"
              sx={{ bgcolor: red[500] }}
              aria-label="recipe"
            >
              R
            </Avatar>
          }
          title={
            <Link color="inherit" href="/books">
              Book
            </Link>
          }
          subheader={
            <>
              <Link color="inherit" href="/js">
                JavaScript
              </Link>
              {", "}
              <Link color="inherit" href="/react">
                React
              </Link>
              {", "}
              <Link color="inherit" href="/wordpress">
                Wordpress
              </Link>
            </>
          }
        />
        <CardMedia
          component="a"
          href="https://source.unsplash.com/random"
          rel="noopener noreferrer"
          style={{ height: 0, paddingTop: "56.25%" }}
          image="https://source.unsplash.com/random"
          alt="random"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            Heading
          </Typography>
          <Typography variant="body2" color="text.secondary">
            This impressive paella is a perfect party dish and a fun meal to
            cook together with your guests. Add 1 cup of frozen peas along with
            the mussels, if you like.
          </Typography>
        </CardContent>
        <CardActions disableSpacing>
          <Checkbox
            aria-label="recipe"
            icon={<FavoriteBorder />}
            checkedIcon={<Favorite />}
          />
          <Rating name="size-small" defaultValue={2} size="small" />
        </CardActions>
      </Card>
    </Grid>
  );
};

export default CatalogItem;
