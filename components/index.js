// Layout
export { default as Header } from "./layouts/Header/Header";
export { default as Hero } from "./layouts/Hero/Hero";
export { default as Filters } from "./layouts/Filters/Filters";
export { default as Catalog } from "./layouts/Catalog/Catalog";
export { default as Footer } from "./layouts/Footer/Footer";

// Blocks
export { default as CatalogItem } from "./blocks/CatalogItem/CatalogItem";
