import React from "react";
import { CatalogItem } from "../../../components/index";
import { Grid } from "@mui/material";

const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];

const Catalog = () => {
  return (
    <Grid container spacing={2}>
      {cards.map((card) => (
        <CatalogItem key={card} />
      ))}
    </Grid>
  );
};

export default Catalog;
