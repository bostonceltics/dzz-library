import React from "react";
import {
  Box,
  Typography,
  FormGroup,
  FormControlLabel,
  Checkbox,
  Divider,
} from "@mui/material";

const Filters = () => {
  return (
    <Box
      height="100%"
      sx={{
        bgcolor: "background.paper",
        py: 3,
        px: 2,
      }}
    >
      <Typography variant="h6" gutterBottom>
        Type:
      </Typography>
      <FormGroup>
        <FormControlLabel
          control={<Checkbox defaultChecked />}
          label={<Typography variant="subtitle2">Book</Typography>}
        />
        <FormControlLabel
          control={<Checkbox />}
          label={<Typography variant="subtitle2">Course</Typography>}
        />
        <FormControlLabel
          control={<Checkbox />}
          label={<Typography variant="subtitle2">Webinar</Typography>}
        />
      </FormGroup>
      <Divider sx={{ my: 2 }} />
      <Typography variant="h6" gutterBottom>
        Department:
      </Typography>
      <FormGroup>
        <FormControlLabel
          control={<Checkbox defaultChecked />}
          label={<Typography variant="subtitle2">Managers</Typography>}
        />
        <FormControlLabel
          control={<Checkbox />}
          label={<Typography variant="subtitle2">Designers</Typography>}
        />
        <FormControlLabel
          control={<Checkbox />}
          label={<Typography variant="subtitle2">Programmers</Typography>}
        />
      </FormGroup>
      <Divider sx={{ my: 2 }} />
      <Typography variant="h6" gutterBottom>
        Category:
      </Typography>
      <FormGroup>
        <FormControlLabel
          control={<Checkbox defaultChecked />}
          label={<Typography variant="subtitle2">JavaScript</Typography>}
        />
        <FormControlLabel
          control={<Checkbox />}
          label={<Typography variant="subtitle2">PHP</Typography>}
        />
        <FormControlLabel
          control={<Checkbox />}
          label={<Typography variant="subtitle2">React</Typography>}
        />
      </FormGroup>
    </Box>
  );
};

export default Filters;
