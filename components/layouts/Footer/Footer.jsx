import React from "react";
import { Box, Typography } from "@mui/material";

const Footer = () => {
  return (
    <Box sx={{ bgcolor: "background.paper", p: 6 }} component="footer">
      <Typography variant="body2" color="text.secondary" align="center">
        © 2006-{new Date().getFullYear()} Dizzain Inc. All Rights Reserved
        <br />
        Sunnyvale + New York Web Design Company
      </Typography>
    </Box>
  );
};

export default Footer;
