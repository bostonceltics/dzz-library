import React from "react";
import { Box, Typography, Container, Stack, Button } from "@mui/material";

const Hero = () => {
  return (
    <Box
      sx={{
        bgcolor: "background.paper",
        pt: 12,
        pb: 6,
        mb: 6,
      }}
    >
      <Container maxWidth="md">
        <Typography
          component="h1"
          variant="h2"
          align="center"
          color="text.primary"
          gutterBottom
        >
          Dizzain library
        </Typography>

        <Typography
          variant="h5"
          align="center"
          color="text.secondary"
          paragraph
        >
          {`Unlock the library's resources by signing in with your corporate
              email address.`}
        </Typography>
        <Stack
          sx={{ pt: 4 }}
          direction="row"
          spacing={2}
          justifyContent="center"
        ></Stack>
      </Container>
    </Box>
  );
};

export default Hero;
